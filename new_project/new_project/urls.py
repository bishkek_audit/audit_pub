from django.conf.urls import include, url
from django.contrib import admin
from django.conf.urls.static import static
from django.views.generic import RedirectView
from new_project import settings

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include('projects.urls')),
    url(r'^auth/', include('login_auth.urls')),
    url(r'^$', RedirectView.as_view(url = '/projects/projects/', permanent = True)),
] + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)