import os
from celery import Celery
from new_project import settings
 
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'new_project.settings')
 
app = Celery('new_project')
app.config_from_object('django.conf:settings')
 
# Load task modules from all registered Django app configs.
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)