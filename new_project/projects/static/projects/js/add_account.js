$(document).ready(function() {
    $("tbody tr").click(function(){
        if($(this).hasClass('selected')){
            $(this).removeClass('selected');
            $(this).find(".green_ok").removeClass('glyphicon glyphicon-ok');  
            var accounts = "";
            $(".all-accounts").find('tr').each(function (i, el) {
                if($(this).hasClass('selected'))
                {
                    var $tds = $(this).find('td'), account = $.trim($tds.eq(1).text());
                    accounts += account+", ";
                }
            });
            accounts = accounts.replace(/([,\s]+)$/g, '');
            add_account(rep_name, accounts, proj_id);
        } else {
            $(this).addClass('selected');
            $(this).find(".green_ok").addClass('glyphicon glyphicon-ok');
            var accounts = "";
            $(".all-accounts").find('tr').each(function (i, el) {
                if($(this).hasClass('selected'))
                {
                    var $tds = $(this).find('td'), account = $.trim($tds.eq(1).text());
                    accounts += account+", ";
                }
            });
            accounts = accounts.replace(/([,\s]+)$/g, '');
            add_account(rep_name, accounts, proj_id);
        }
    });

    $(document).on('click', '#back', function() {
        window.location.replace("../../projects/"+proj_id);
    });

    function add_account(rep_name, acc_arr, proj_id){
       $.ajax({
            url : '/add_accounts',
            type : "POST",
            data : {
                pr_id : proj_id,
                name : rep_name,
                accounts : acc_arr
            },
            beforeSend: function(xhr, settings) {
                if (settings.type == 'POST' || settings.type == 'PUT' || settings.type == 'DELETE') {
                    function getCookie(name) {
                        var cookieValue = null;
                        if (document.cookie && document.cookie != '') {
                            var cookies = document.cookie.split(';');
                            for (var i = 0; i < cookies.length; i++) {
                                var cookie = jQuery.trim(cookies[i]);
                                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                                    break;
                                }
                            }
                        }
                        return cookieValue;
                    }
                    if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                        xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                    }
                }
            },
            success : function(json) {
                if (json.message == "SUCCESS") {
                    console.log("OK!");
                }
                else{ 
                    alert(json.error); 
                }
            },
            error : function(xhr,errmsg,err) {
                //alert(xhr.status + ": " + xhr.responseText);
            }
        });
    };
});