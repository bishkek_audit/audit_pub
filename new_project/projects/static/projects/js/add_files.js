$(document).ready(function() {
	$("#id_file_path").change(function(){
        var filename = $('input[type=file]').val().replace(/.*(\/|\\)/, '').replace(".zip", "");
		$('input[name="table_name"]').val(filename);
    });

    $('#upload_file').on('submit', function(event){
		event.preventDefault();
		file = new FormData($('#upload_file')[0]);
        $("#buttonSubmit").prop("disabled", true);
		$.ajax({
	        url: '/upload_file/' + $('#id_project').val(),
	        type: 'POST',
	        data: file,
	        cache: false,
	        contentType: false,
	        processData: false,
	        beforeSend: function(xhr, settings) {
	        	$(".processing").show();
	            if (settings.type == 'POST' || settings.type == 'PUT' || settings.type == 'DELETE') {
	                function getCookie(name) {
	                    var cookieValue = null;
	                    if (document.cookie && document.cookie != '') {
	                        var cookies = document.cookie.split(';');
	                        for (var i = 0; i < cookies.length; i++) {
	                            var cookie = jQuery.trim(cookies[i]);
	                            if (cookie.substring(0, name.length + 1) == (name + '=')) {
	                                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
	                                break;
	                            }
	                        }
	                    }
	                    return cookieValue;
	                }
	                if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
	                    xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
	                }
	            }
	        },
	        success : function(json) {
	        	if(json[0] == "Success"){
                    for (var i = 1; i < json.length; i++) {
                        $("#id_file_path").val('');
                        var status, tn, st;
                        $(".processing").hide();
                        $("#buttonSubmit").prop("disabled", false);
                        if(json[i][2] == "chart_of_accounts") {
                            tn = "Accounts";
                        }
                        else if (json[i][2] == "balance") {
                            tn = "Balance";
                        }
                        else {
                            tn = "Turnovers";
                        }
                        if (json[i][1] == 'pending' || json[i][1] == 'processing') st = 'info';
                        if (json[i][1] == 'failed') st = 'danger';
                        if (json[i][1] == 'success') st = 'success';
                        status = "<div id=\"st_"+json[i][2]+"_"+json[i][0]+"\" class=\"btn alert-"+st+" btn-sm\">"+json[i][1]+"</div>";
                        $("#no_doc_"+json[i][2]).remove();
                        $("#"+json[i][2]).append(
                            "<tr id=\"doc_"+json[i][0]+"\"><td style=\"vertical-align: middle;\"><a class=\"btn btn-default btn-sm\" >"+tn
                            +"</a></td><td style=\"vertical-align: middle;\">"+status
                            +"</td><td><form id=\""+json[i][0]+"\" class=\"delete_file_form\" table-name=\""+json[i][2]
                            +"\"method=\"POST\"><input type=\"hidden\" name=\"csrfmiddlewaretoken\" value=\""+csrftoken
                            +"\"><input type=\"submit\" value=\"Delete\" class=\"btn btn-danger btn-sm\" style=\"float: right;\"/></form></td></tr>"
                            ).on('click', '.delete_file_form', function (e) {
                                e.preventDefault();
                                var file_id = $(this).attr('id');
                                var tbl_name = $(this).attr('table-name');
                                delete_file(file_id, tbl_name);
                            });
                    }
	        	} else {
                    $(".processing").hide();
                    $("#buttonSubmit").prop("disabled", false);
                    $('#id_file_path').val('');
                    alert('Error with upload archive: '+json.message);
                }
	        },
	        error : function(xhr,errmsg,err) {
                $(".processing").hide();
                $("#buttonSubmit").prop("disabled", false);
	            $('#results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+errmsg+"</div>");
	            console.log(xhr.status + ": " + xhr.responseText);
	        }
	    });
    });

    $('.delete_file_form').on('submit', function(event){
        event.preventDefault();
        var file_id = $(this).attr('id');
        var tbl_name = $(this).attr('table-name');
        delete_file(file_id, tbl_name);
    });

    $('.delete_transactions').on('submit', function(event){
        event.preventDefault();
        var tbl_name = $(this).attr('table-name');
        var trans_file_path = $("#id_for_del_trans").val();
        delete_trans(tbl_name, trans_file_path);
    });

    $('input[data-type-check="check"]').on('click', function(){
        /*if($(this).data('name') == 'checkall'){
            var aa = document.getElementsByTagName("input");
            for (var i =0; i < aa.length; i++){
                if (aa[i].type == 'checkbox' && aa[i].disabled == false && aa[i].checked == false)
                {
                    console.log(aa[i]);
                    aa[i].checked = true;
                }
            }
        }*/

        if($(this).is(':checked')){
        	$(this).attr("checked","");
            check_checkbox($(this).data('name'), $(this).data('type-name'), $(this).data('type-name'));
        } else {
        	$(this).removeAttr("checked","");
            check_checkbox($(this).data('name'), $(this).data('type-name'), '');
        }
    });

    $('#start_reports').on('submit', function(event){
        event.preventDefault();
        var account_template = $('#account_template').val();
        var defaultStart = moment().startOf('year').format('YYYY-MM-DD HH:mm:ss');
        var defaultEnd = moment(new Date().valueOf()).format('YYYY-MM-DD HH:mm:ss');
        var materiality = $('#materiality').val() != '' ? $('#materiality').val() : 0;
        var daterange = $('#daterange').val() == '' ? defaultStart +" - "+ defaultEnd : $('#daterange').val();
        var ex_accounts = '';
        if ($('#ex_accounts').val() == '') {
            ex_accounts = 'no_ex_acc';
        } else {
            for (var i=0; i < $('#ex_accounts').val().length; i++) {
                ex_accounts += $('#ex_accounts').val()[i]+', ';
            }
            ex_accounts = ex_accounts.replace(/([,\s]+)$/g, '');
        }
        $("#reports").find('tr').each(function (i, el) {
            var $rep_name = $(this).find('a'), types = "";
            var name = $.trim($rep_name.eq(0).text());
            var $checkboxes = $(this).find(':checked');
            for (var i = 0; i < $checkboxes.length; i++) {
                types += $checkboxes.eq(i).data('type-name')+", ";
            }
            types = types.replace(/([,\s]+)$/g, '');
            if (types) {
                start(name, types, materiality, 'no_ex_acc', daterange, account_template);
                $("#button_start").prop("disabled", true);
                /*var test = $("#rep_"+name).text();
                    console.log(test, name);
                    if(test == name) {
                    } else {
                        alert("Report "+name+" start");
                }*/
            }
        });
        $("#reports2").find('tr').each(function (i, el) {
            var $rep_name = $(this).find('a'), r_types = "";
            var r_name = $.trim($rep_name.eq(0).text());
            var $checkboxes = $(this).find(':checked');
            for (var i = 0; i < $checkboxes.length; i++) {
                r_types += $checkboxes.eq(i).data('type-name')+", ";
            }
            r_types = r_types.replace(/([,\s]+)$/g, '');
            if (r_types) {
                start(r_name, r_types, materiality, ex_accounts, daterange, account_template);
                $("#button_start").prop("disabled", true);
            }
        });
    });

    $('#start_reports_tr').on('submit', function(event){
        event.preventDefault();
        $("#button_start_tr").prop("disabled", true);
        start_transactions();
    });

    $("#reports").find('tr').each(function (i, el) {
        var $rep_name = $(this).find('a');
        var $checkboxes = $(this).find('input[type=checkbox]');
        if($rep_name.hasClass('btn-info')){
            $checkboxes.prop('disabled', false);
        }
        else{
            $checkboxes.removeAttr("checked", "");
        }
    });

    $("#reports2").find('tr').each(function (i, el) {
        var $rep_name = $(this).find('a');
        var $checkboxes = $(this).find('input[type=checkbox]');
        if($rep_name.hasClass('btn-info')){
            $checkboxes.prop('disabled', false);
        }
        else{
            $checkboxes.removeAttr("checked", "");
        }
    });

    if($('#no_doc_transactions').length){
        $("#button_start_tr").prop("disabled", false);
    }else{
        $("#button_start_tr").prop("disabled", true);
    }
});

function delete_trans(tbl_name, trans_file_path){
    $.ajax({
        url : '/delete_transactions',
        type : "POST",
        data : {
            pr_id: $('#id_project').val(),
            tr_tbl_name: tbl_name,
            tr_file_path : trans_file_path
        },
        beforeSend: function(xhr, settings) {
            if (settings.type == 'POST' || settings.type == 'PUT' || settings.type == 'DELETE') {
                function getCookie(name) {
                    var cookieValue = null;
                    if (document.cookie && document.cookie != '') {
                        var cookies = document.cookie.split(';');
                        for (var i = 0; i < cookies.length; i++) {
                            var cookie = jQuery.trim(cookies[i]);
                            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                                break;
                            }
                        }
                    }
                    return cookieValue;
                }
                if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                    xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                }
            }
        },
        success : function(json) {
            if(json.status == "success"){
                $("#doc_transactions").remove();
                $("#trans_status").text("Status");
                $("#body_transactions").append(
                    "<tr id=\"no_doc_transactions\" class=\"alert alert-info btn-sm\"><td>No documents.</td><td></td><td></td></tr>"
                );
            $("#button_start_tr").prop("disabled", false);
            }
            else
            {
                alert(json.msg);
            }
        },
        error : function(xhr,errmsg,err) {
            $('#results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+errmsg+"</div>");
            console.log(xhr.status + ": " + xhr.responseText);
        }
    });
};

function start_transactions(){
   $.ajax({
        url : '/start_transactions',
        type : "POST",
        data : {
            pr_id: $('#id_project').val(),
            r_name: "transactions"
        },
        beforeSend: function(xhr, settings) {
            if (settings.type == 'POST' || settings.type == 'PUT' || settings.type == 'DELETE') {
                function getCookie(name) {
                    var cookieValue = null;
                    if (document.cookie && document.cookie != '') {
                        var cookies = document.cookie.split(';');
                        for (var i = 0; i < cookies.length; i++) {
                            var cookie = jQuery.trim(cookies[i]);
                            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                                break;
                            }
                        }
                    }
                    return cookieValue;
                }
                if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                    xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                }
            }
        },
        success : function(json) {
            if($("#no_doc_transactions")){
                $("#no_doc_transactions").remove()
            }
            if (json.message == 'OK') {
                $("#body_transactions").append(
                    "<tr id=\"doc_transactions\"><td><div class=\"btn btn-default btn-sm\">"+
                    "<b>Transactions</b></div></td><td><div id=\"down_transactions\"><div class=\"btn alert-info btn-sm\">"+
                    "Pending</div></div></td><td><form class=\"delete_transactions\" table-name=\"transactions\" method=\"POST\">"+
                    "<input type=\"submit\" value=\"Delete\" class=\"btn btn-danger btn-sm\">"+csrftoken+"</form></td></tr>"
                ).on('click', '.delete_transactions', function (e) {
                    e.preventDefault();
                    var tbl_name = $(this).attr('table-name');
                    var trans_file_path = $("#id_for_del_trans").val();
                    delete_trans(tbl_name, trans_file_path);
                });
            }else {
                alert("Error with "+json.r_name+"_report, try again!")
            }
        },
        error : function(json, xhr, errmsg, err) {
            $("#button_start_tr").prop("disabled", false);
        }
    });
};

function delete_file(file_id, tbl_name){
    $.ajax({
        url : '/delete_file/'+file_id,
        type : "POST",
        data : { 
            title_post : $('#project_title').val() 
        },
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function(xhr, settings) {
            if (settings.type == 'POST' || settings.type == 'PUT' || settings.type == 'DELETE') {
                function getCookie(name) {
                    var cookieValue = null;
                    if (document.cookie && document.cookie != '') {
                        var cookies = document.cookie.split(';');
                        for (var i = 0; i < cookies.length; i++) {
                            var cookie = jQuery.trim(cookies[i]);
                            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                                break;
                            }
                        }
                    }
                    return cookieValue;
                }
                if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                    xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                }
            }
        },
        success : function(json) {
            $("#doc_"+file_id).remove();
            var rowCount = $('#'+tbl_name+' >tr').length;
            if (rowCount < 1) {
                $("#"+tbl_name).append(
                    "<tr id=\"no_doc_"+tbl_name+"\" class=\"alert alert-info btn-sm\"><td>No documents.</td><td></td><td></td></tr>"
                );
            }
        },
        error : function(xhr,errmsg,err) {
            $('#results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+errmsg+"</div>");
            console.log(xhr.status + ": " + xhr.responseText);
        }
    });
};

function start(name, types, materiality, ex_accounts, daterange, account_template){
   $.ajax({
        url : '/start',
        type : "POST",
        data : {
            pr_id : $('#id_project').val(),
            r_name : name,
            r_type : types,
            materiality: materiality,
            ex_acc: ex_accounts,
            date: daterange,
            acc_template: account_template
        },
        beforeSend: function(xhr, settings) {
            $(".processing_"+name).show();
            if($("#rep_"+name)){
                $("#rep_"+name).remove()
            }
            if (settings.type == 'POST' || settings.type == 'PUT' || settings.type == 'DELETE') {
                function getCookie(name) {
                    var cookieValue = null;
                    if (document.cookie && document.cookie != '') {
                        var cookies = document.cookie.split(';');
                        for (var i = 0; i < cookies.length; i++) {
                            var cookie = jQuery.trim(cookies[i]);
                            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                                break;
                            }
                        }
                    }
                    return cookieValue;
                }
                if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                    xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                }
            }
        },
        success : function(json) {
            $(".processing_"+name).hide();
            $("#button_start").prop("disabled", false);
            if($("#rep_"+name)){
                $("#rep_"+name).remove()
            }
            if (json.message == 'OK') {
                $("#td_"+json.r_name).append(
                    "<div id='rep_"+json.r_name+"'><span class='btn alert-info btn-sm'>Pending</span></div>");
            }else {
                $("#td_"+json.r_name).append(
                    "<div id='rep_"+json.r_name+"' tooltip='"+json.msg+"'><span class='btn-sm btn-danger'>Failed</span></div>");
                    //alert("Error "+json.msg+" with "+json.r_name+"_report, try again!")
            }
        },
        error : function(json, xhr, errmsg, err) {
            $(".processing_"+name).hide();
            $("#button_start").prop("disabled", false);
        }
    });
};

function check_checkbox(name, type, checked){
   $.ajax({
        url : '/check_checkbox',
        type : "POST",
        data : {
            pr_id : $('#id_project').val(),
            r_name : name,
            r_type : type,
            r_checked : checked
        },
        beforeSend: function(xhr, settings) {
            if (settings.type == 'POST' || settings.type == 'PUT' || settings.type == 'DELETE') {
                function getCookie(name) {
                    var cookieValue = null;
                    if (document.cookie && document.cookie != '') {
                        var cookies = document.cookie.split(';');
                        for (var i = 0; i < cookies.length; i++) {
                            var cookie = jQuery.trim(cookies[i]);
                            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                                break;
                            }
                        }
                    }
                    return cookieValue;
                }
                if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                    xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                }
            }
        },
        success : function(json) {
            console.log('OK!');
        },
        error : function(xhr,errmsg,err) {
            alert(xhr.status + ": " + xhr.responseText);
        }
    });
};

function validate(evt) {
  var theEvent = evt || window.event;
  var key = theEvent.keyCode || theEvent.which;
  key = String.fromCharCode( key );
  var regex = /[0-9]|\./;
  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    if(theEvent.preventDefault) theEvent.preventDefault();
  }
};
