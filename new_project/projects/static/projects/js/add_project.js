$(document).ready(function() {
    $('#add_project_form').on('submit', function(event){
        event.preventDefault();
        $.ajax({
            url : '/add_project',
            type : "POST",
            data : { 
                title_post : $('#project_title').val() 
            },
            beforeSend: function(xhr, settings) {
                if (settings.type == 'POST' || settings.type == 'PUT' || settings.type == 'DELETE') {
                    function getCookie(name) {
                        var cookieValue = null;
                        if (document.cookie && document.cookie != '') {
                            var cookies = document.cookie.split(';');
                            for (var i = 0; i < cookies.length; i++) {
                                var cookie = jQuery.trim(cookies[i]);
                                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                                    break;
                                }
                            }
                        }
                        return cookieValue;
                    }
                    if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                        xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                    }
                }
            },
            success : function(json) {
                if(json.message) {
                    $("#project_title").val("");
                    $("#talk").append("<a href=\"/projects/"+json.id+"\" class=\"list-group-item list-group-item-action\">"+json.title+"</a>");
                } else {
                    alert(json.error);
                    console.log(json.error);
                }
            },
            error : function(xhr,errmsg,err) {
                $('#results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+errmsg+"</div>");
                console.log(xhr.status + ": " + xhr.responseText);
            }
        });
    });
});