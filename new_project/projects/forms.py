from django import forms
from django.contrib import auth
from .models import Project, Document

class AddProjectForm(forms.ModelForm):
	class Meta:
		model = Project
		fields = ["title"]
		widgets = {'title': forms.TextInput( attrs = {'id' :  'project_title'})}

class UploadDocumentForm(forms.ModelForm):
	project = forms.ModelChoiceField(queryset=Project.objects.all(), widget=forms.HiddenInput())
	class Meta:
		model = Document
		fields = ["project", "file_path", "table_name"]
		widgets = {'file_path':  forms.FileInput(attrs={
						'accept':'.zip',
						'style':'float:left'}),
					'table_name': forms.HiddenInput()}