import os
from django.contrib.auth.models import User
from django.db import models
from model_utils import Choices

class Project(models.Model):
   title = models.CharField("Project", max_length = 200)
   date = models.DateTimeField("Date", auto_now_add = True)
   user = models.ForeignKey(User, on_delete = models.CASCADE,)
         
   class Meta:
      verbose_name = "Проекты"      
      verbose_name_plural = "Проекты"

class Projects_table(models.Model):
   table_name = models.CharField("Table_name", max_length = 200)
   pr = models.ForeignKey(Project, on_delete = models.CASCADE,)

class Document(models.Model):
   project = models.ForeignKey(Project, on_delete = models.CASCADE,)
   file_path = models.FileField(upload_to = "documents/%Y/%m/%d")
   STATUS = Choices('pending', 'processing', 'success', 'failed', 'delete')
   status = models.CharField(choices=STATUS, default=STATUS.pending, max_length=20)
   table_name = models.CharField("table_name", max_length = 25, null=True)
   err_message = models.CharField("err_message", max_length = 1500, null=True)
   created_on = models.DateTimeField(auto_now_add=True, null=True)

   def filename(self):
      return os.path.basename(self.file_path.name)

   class Meta:
      verbose_name = "Файлы"      
      verbose_name_plural = "Файлы"

class Reports_name(models.Model):
   full_name = models.CharField("full_name", max_length = 100)
   short_name = models.CharField("short_name", max_length = 20)
   balance = models.BooleanField(default=False)
   proffit_loss = models.BooleanField(default=False)

   class Meta:
      verbose_name = "Названия отчетов"      
      verbose_name_plural = "Названия отчетов"

class Reports_type(models.Model):
   name = models.CharField("name", max_length = 200)
   balance = models.BooleanField(default=False)
   proffit_loss = models.BooleanField(default=False)
   query = models.TextField(null=True)

   class Meta:
      verbose_name = "Тип отчета"      
      verbose_name_plural = "Тип отчета"