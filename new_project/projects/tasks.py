from django.db import connection
from django.shortcuts import get_object_or_404
from django.http import HttpResponse
from new_project.celery import app
from projects.models import Document, Project, Reports_type, Reports_name
from new_project import settings
import csv, os, time, zipfile, json, openpyxl, decimal, sys#, pickle, shutil

def execute(commands):
    cur = None
    try:
        cur = connection.cursor()
        for command in commands:
            cur.execute(command)
        cur.close()
        result = True
    except Exception as e:
        print("function execute error: "+str(e))
        result = str(e)
    finally:
        if cur is not None:
            cur.close()
    return result

@app.task
def create_tables(project):
	rep_name = ""
	reports_name = Reports_name.objects.all()
	for r_n in reports_name:
		rep_name += "('%s', ''),\n"%r_n.short_name
	
	rep_type = ""
	reports_type = Reports_type.objects.all()
	for r_t in reports_type:
		rep_type += ", %s VARCHAR DEFAULT ''\n"%r_t.name

	commands = (
		"""
    	CREATE TABLE balance_{id} (
            Account VARCHAR,
            Counterparty text,
            Objective text,
            Specification text,
            Amount DOUBLE PRECISION,
            CCY VARCHAR,
            Quantity VARCHAR
        )
        """.format(id=str(project.id)),
        """
    	CREATE INDEX bal_acc_counter_{id} ON balance_{id} (Account, Counterparty);
    	CREATE INDEX bal_account_{id} ON balance_{id} (Account);
    	CREATE INDEX bal_amount_{id} ON balance_{id} (Amount);
        """.format(id=str(project.id)),
    	"""
    	CREATE TABLE chart_of_accounts_%d (
            Account VARCHAR,
            Description text
        )
        """ % (project.id),
        """
    	CREATE INDEX coa_account_{id} ON chart_of_accounts_{id} (Account)
        """.format(id=str(project.id)),
    	"""
    	CREATE TABLE turnovers_{id} (
            DATE_TIME TIMESTAMP,
            DR VARCHAR,
            CR VARCHAR,
            Document text,
            Description text,
            Objective text,
            Specification text,
            AmountNCY DOUBLE PRECISION,
            AmountDrCCY text,
            DrCCY text,
            AmountCrCCY DOUBLE PRECISION,
            CrCCY VARCHAR,
            UserId VARCHAR
        )
        """.format(id=str(project.id)),
    	"""
    	CREATE INDEX tur_DATE_TIME_DR_{id} ON turnovers_{id} (DR, DATE_TIME);
    	CREATE INDEX tur_DATE_TIME_CR_{id} ON turnovers_{id} (CR, DATE_TIME);
    	CREATE INDEX tur_AmountNCY_{id} ON turnovers_{id} (AmountNCY);
        """.format(id=str(project.id)),
    	"""
    	CREATE TABLE reports_process_{id} (
			REPORT_NAME VARCHAR,
            file_path VARCHAR,
            err_message VARCHAR,
			file_add BOOLEAN DEFAULT False,
			ACCOUNTS TEXT
			{rep_type},
			CREATED_ON TIMESTAMP DEFAULT current_timestamp
        );
    	CREATE INDEX REPORT_NAME_{id} ON reports_process_{id} (REPORT_NAME);
        """.format(id=str(project.id), rep_type=str(rep_type)),
        """
        INSERT INTO projects_projects_table(table_name, pr_id)
			VALUES 
				('turnovers_%d'""" % (project.id) + """, %d""" % project.id + """),
				('chart_of_accounts_%d'""" % (project.id) + """, %d""" % project.id + """),
				('balance_%d'""" % (project.id) + """, %d""" % project.id + """),
				('reports_process_%d'""" % (project.id) + """, %d""" % project.id + """);
        """,
        """
        INSERT INTO reports_process_%d(report_name, accounts)
        	VALUES %s;
        """ % (project.id, rep_name[:-2]),
        """
        INSERT INTO reports_process_%d(report_name, accounts)
        	VALUES %s;
        """ % (project.id, "('transactions', '')")
        )
	return execute(commands)

@app.task
def delete_tables(project_id):
	commands = (
		"""
		DROP TABLE IF EXISTS turnovers_%s;
		""" % (project_id),
		"""
		DROP TABLE IF EXISTS chart_of_accounts_%s;
		""" % (project_id),
		"""
		DROP TABLE IF EXISTS balance_%s;
		""" % (project_id),
		"""
		DROP TABLE IF EXISTS reports_process_%s;
		""" % (project_id),
		"""
		DROP TABLE IF EXISTS reports_document_%s;
		""" % (project_id)
		)
	return execute(commands)

@app.task
def import_to_db(table_name, doc_exp, doc_arch):
	try:
		doc_exp.status = "processing"
		doc_exp.save()
		delete_data(table_name)
		f_path = settings.MEDIA_ROOT+"/%s"%doc_exp.file_path
		result = process_file(table_name, f_path)
		if result == True:
			doc_exp.status = "success"
			doc_exp.save()
			delete_ex_file.delay(doc_exp, doc_arch)
			return True
		else:
			doc_exp.status = "failed"
			doc_exp.err_message = result
			doc_exp.save()
			delete_ex_file.delay(doc_exp, doc_arch)
			return False
	except Exception as e:
		print("function import_to_db error: "+str(e))
		doc_exp.status = "failed"
		doc_exp.err_message = str(e)
		doc_exp.save()
		delete_ex_file.delay(doc_exp, doc_arch)
		return False

def delete_data(table_name):
	command = (
		"""DELETE FROM %s;""" % table_name,
		)
	return execute(command)

def process_file(table_name, f_path):
	command = (
		"""COPY %s FROM '%s' WITH DELIMITER ';' CSV HEADER;""" % (table_name, f_path),
		)
	return execute(command)

@app.task
def extract_file(document, fileIDs):
	result = False
	document_name, document_ext = os.path.splitext(document.filename())
	base_dir = os.path.dirname(os.path.dirname(__file__))+document.file_path.url.replace(document.filename(), document_name)
	path = os.path.dirname(os.path.dirname(__file__))+document.file_path.url
	cwd = os.getcwd()
	try:
		opener, mode = zipfile.ZipFile, 'r'
		if not os.path.exists(base_dir):
			os.makedirs(base_dir)
		os.chdir(base_dir)
		file = opener(path, mode)
		for file_id in fileIDs:
			doc_exp = Document.objects.get(id=file_id)
			file.extract(doc_exp.table_name+".csv")
			import_to_db.delay(str(doc_exp.table_name)+"_"+str(doc_exp.project_id), doc_exp, document)
		file.close()
		document.status = "success"
		document.save()
		result = True
	except Exception as e:
		print("function extract_file error: "+str(e))
		document.status = "failed"
		document.save()
		for file_id in fileIDs:
			doc_exp = Document.objects.get(id=file_id)
			if doc_exp.status == "pending" or doc_exp.status == "processing":
				doc_exp.status = "failed"
				doc_exp.err_message = str(e)
				doc_exp.save()
		result = False
	finally:
		os.chdir(cwd)
		document.status = "executed"
		document.save()
	return result

@app.task
def delete_ex_file(document, folder):
	try:
		doc_name, doc_ext = os.path.splitext(folder.filename())
		base_dir = os.path.dirname(os.path.dirname(__file__))+folder.file_path.url.replace(folder.filename(), doc_name)
		file = os.path.dirname(os.path.dirname(__file__))+document.file_path.url
		if os.path.exists(file):
			os.remove(file)
		else:
			print("File is not exists")
		if not os.listdir(base_dir):
			os.rmdir(base_dir)
		else:
			print("Directory is not empty")
	except Exception as e:
		print("function delete_ex_file error: "+str(e))
		return False
	finally:
		return True

@app.task
def transactions(r_name, project):
    try: 
        commands = (
            """SELECT * FROM turnovers_{id} WHERE amountncy < 0 LIMIT 100000;""".format(id=str(project.id)),
            """SELECT * FROM turnovers_{id} WHERE MOD(cast(amountncy as numeric),cast(100000 as numeric))=0 AND amountncy>0 LIMIT 200000;""".format(id=str(project.id)),
            """SELECT * FROM turnovers_{id} WHERE (Select Extract(MONTH from date_time)) < 7 AND (description LIKE '%октяб%' OR description LIKE '%декаб%' OR description LIKE '%нояб%' OR document LIKE '%октяб%' OR document LIKE '%декаб%' OR document LIKE '%нояб%' OR specification LIKE '%октяб%' OR specification LIKE '%декаб%' OR specification LIKE '%нояб%' OR objective LIKE '%октяб%' OR objective LIKE '%декаб%' OR objective LIKE '%нояб%') LIMIT 200000;""".format(id=str(project.id)),
            """SELECT * FROM turnovers_{id} WHERE (dr, cr) IN (SELECT dr, cr FROM turnovers_{id} GROUP BY dr, cr HAVING COUNT(*) < 13) LIMIT 200000;""".format(id=str(project.id)),
            """SELECT * FROM turnovers_{id} WHERE EXTRACT(DAY FROM date_time)=30 OR EXTRACT(DAY FROM date_time)=31 LIMIT 100000;""".format(id=str(project.id)),
            """SELECT * FROM turnovers_{id} WHERE EXTRACT(ISODOW FROM date_time)=6 LIMIT 100000;""".format(id=str(project.id)),
            """SELECT * FROM turnovers_{id} WHERE EXTRACT(ISODOW FROM date_time)=7 LIMIT 100000;""".format(id=str(project.id)),
            """SELECT * FROM turnovers_{id} WHERE EXTRACT(HOUR FROM date_time)>19 OR EXTRACT(HOUR FROM date_time)<8 LIMIT 200000;""".format(id=str(project.id)),
            """SELECT * FROM turnovers_{id} WHERE (EXTRACT(DAY FROM date_time)=1 AND EXTRACT(MONTH FROM date_time)=1) or (EXTRACT(DAY FROM date_time)=7 AND EXTRACT(MONTH FROM date_time)=1) or (EXTRACT(DAY FROM date_time)=23 AND EXTRACT(MONTH FROM date_time)=2) or (EXTRACT(DAY FROM date_time)=8 AND EXTRACT(MONTH FROM date_time)=3) or (EXTRACT(DAY FROM date_time)=21 AND EXTRACT(MONTH FROM date_time)=3) or (EXTRACT(DAY FROM date_time)=1 AND EXTRACT(MONTH FROM date_time)=5) or (EXTRACT(DAY FROM date_time)=9 AND EXTRACT(MONTH FROM date_time)=5) or (EXTRACT(DAY FROM date_time)=31 AND EXTRACT(MONTH FROM date_time)=8) or (EXTRACT(DAY FROM date_time)=7 AND EXTRACT(MONTH FROM date_time)=11) LIMIT 200000;""".format(id=str(project.id)),
            """SELECT * FROM turnovers_{id} WHERE description LIKE '%коррек%' or description LIKE '%реверс%' or description LIKE '%сторно%' or description LIKE '%обрат%' or description LIKE '%возвра%' or objective LIKE '%коррек%' or objective LIKE '%реверс%' or objective LIKE '%сторно%' or objective LIKE '%обрат%' or objective LIKE '%возвра%' or specification LIKE '%коррек%' or specification LIKE '%реверс%' or specification LIKE '%сторно%' or specification LIKE '%обрат%' or specification LIKE '%возвра%' or document LIKE '%коррек%' or document LIKE '%реверс%' or document LIKE '%сторно%' or document LIKE '%обрат%' or document LIKE '%возвра%';""".format(id=str(project.id)),
		)
        cnt = 8
        update_reports_document(r_name, 'Processing', '', True, project)
        for command in commands:
            headers = ['DateTime', 'DR', 'CR', 'Document', 'Description', 'Objective', 'Specification', 'AmountNCY', 'AmountDrCCY', 'DrCCY', 'AmountCrCCY', 'CrCCY', 'UserID']
            get_rec = get_records(command)
            get_rec.insert(0, headers)
            file_path = export(project, "transactions", "", get_rec, cnt)
            cnt+=1
        if file_path[0]:
            update_reports_document(r_name, file_path[1], '', True, project)
            return True
        else:
            update_reports_document(r_name, 'Failed', file_path[1].replace("'", "\""), True, project)
            return False
    except Exception as e:
        print("function transactions error: "+str(e))
        update_reports_document(r_name, 'Failed', str(e).replace("'", "\""), True, project)
        return False

@app.task
def get_reports(project, mat, ex_acc, r_name, r_type, daterange, acc_template):
    try:
        update_reports_document(r_name, 'Processing', '', True, project)
        for r_t in r_type.split(','):
            rt = r_t.replace(' ','')
            if rt == 'Register':
                file_path = register(project, r_name, rt, ex_acc, daterange, acc_template)
            if rt == 'Roll':
                file_path = roll(project, r_name, rt, daterange, acc_template)
            if rt == 'Mon_Roll':
                file_path = monthly_roll(project, r_name, rt, ex_acc, daterange, acc_template)
            elif rt == 'Summary':
                file_path = summary(project, r_name, rt, ex_acc, daterange, acc_template)
            elif rt == 'BrDn':
                file_path = brdn_all(project, r_name, rt, mat, ex_acc, daterange, acc_template)
        if file_path[0]:
            update_reports_document(r_name, file_path[1], '', True, project)
            return True
        else:
            update_reports_document(r_name, 'Failed', file_path[1].replace("'", "\""), True, project)
            delete_report(project.id, r_name)
            return False
    except Exception as e:
        print("function get_reports error: "+str(e))
        update_reports_document(r_name, 'Failed', str(e).replace("'", "\""), True, project)
        delete_report(project.id, r_name)
        return False

def delete_report(project_id, r_name):
    file_path_report = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))+'/media/reports/project_'+str(project_id)+"/"+str(r_name)+"_report.xlsx"
    if os.path.exists(file_path_report):
        os.remove(file_path_report)
        print(file_path_report+" DELETED")
        return True
    else:
        print(file_path_report+" is not exists")
        return False

def register(project, r_name, r_type, ex_acc, daterange, acc_template):
	#if ((openingBalance == (openingBalance + DR - CR)) && (DR => CR))
		#bad_dept = (openingBalance + DR - CR)
	#else
		#bad_dept = 0
    register = []
    if Reports_name.objects.filter(proffit_loss = True).filter(short_name = r_name).exists():
        if ex_acc == 'no_ex_acc':
            result1 = get_data_for_rep(project, r_name, r_type, 0, daterange, acc_template)
            result2 = get_data_for_rep(project, r_name, r_type, 1, daterange, acc_template)
        else:
            result1 = get_data_for_report(project, r_name, r_type, 2, ex_acc, daterange, acc_template)
            result2 = get_data_for_report(project, r_name, r_type, 3, ex_acc, daterange, acc_template)
    else:
        result1 = get_data_for_rep(project, r_name, r_type, 0, daterange, acc_template)
        result2 = get_data_for_rep(project, r_name, r_type, 1, daterange, acc_template)
    i = 0
    s_a = selected_accounts(r_name, project)
    for account in s_a[0][1].split(','):
        register.append(["Accounts", "CounterParty", "Start", "DR", "CR", "End", '      ', 'Bad Dept'])
        acc = account.replace(' ', '')
        for result in result1:
            for res1 in result:
                res1 = list(res1)
                if str(res1[0]) == str(acc):
                    rows = []
                    rows.append(as_text(res1[0]))
                    rows.append(as_text(res1[1]))
                    rows.append(res1[2])
                    rows.append(res1[3])
                    rows.append(res1[4])
                    rows.append(res1[5])
                    rows.append(as_text(res1[6]))
                    rows.append(res1[7])
                    register.append(rows)
        for result in result2:
            for res2 in result:
                res2 = list(res2)
                if str(res2[0]) == str(acc):
                    total = []
                    total.append(as_text('Total'))
                    total.append(as_text(res2[1]))
                    total.append(res2[2])
                    total.append(res2[3])
                    total.append(res2[4])
                    total.append(res2[5])
                    total.append(as_text(res2[6]))
                    total.append(res2[7])
                    register.append(total)
        i += 1
        if i < len(s_a[0][1].split(',')):
            register.append(['', '', '', '', '', '', '', ''])
            register.append(['', '', '', '', '', '', '', ''])
    file_path = export(project, r_name, r_type, register, 0)
    return file_path

def roll(project, r_name, r_type, daterange, acc_template):
    result1 = get_data_for_rep(project, r_name, r_type, 0, daterange, acc_template)
    result2 = get_data_for_rep(project, r_name, r_type, 1, daterange, acc_template)
    result3 = get_data_for_rep(project, r_name, r_type, 2, daterange, acc_template)
    roll = [["Accounts", "Description", "Start"]]
    for result in result2:
        for res in result:
            res = list(res)
            roll[0].append('CR '+res[1])
    roll[0].append("Total Additions")
    for result in result3:
        for res in result:
            res = list(res)
            roll[0].append('DR '+res[1])
    roll[0].append("Total Disposals")
    roll[0].append("Total")
    total_row = []
    s_a = selected_accounts(r_name, project)
    for account in s_a[0][1].split(','):
        acc = account.replace(' ', '')
        row_list = []
        total_start = total_add = total_disp = 0
        for result in result1:
            for res in result:
                res = list(res)
                if str(acc) == str(res[0]):
                    total_start += is_none_value(res[2])
                    row_list.append(res[0])
                    row_list.append(as_text(res[1]))
                    row_list.append(is_none_value(res[2]))
        for result in result2:
            for res2 in result:
                res2 = list(res2)
                if str(acc) == str(res2[0]):
                    total_add += is_none_value(res2[2])
                    row_list.append(is_none_value(res2[2]))
                else:
                    row_list.append(0)
        row_list.append(total_add)
        for result in result3:
            for res3 in result:
                res3 = list(res3)
                if str(acc) == str(res3[0]):
                    total_disp += is_none_value(res3[2])
                    row_list.append(is_none_value(res3[2]))
                else:
                    row_list.append(0)
        row_list.append(total_disp)
        row_list.append(total_start+total_add-total_disp)
        total_row.append(row_list)
        roll.append(row_list)

    total = ['Total', ' ']        
    for tot_row in total_row:
        for j in range(len(tot_row)):
            if j >= 2:
                if j >= len(total):
                    total.append(0)
                total[j] += tot_row[j]
    roll.append(total)
    file_path = export(project, r_name, r_type, roll, 1)
    return file_path
    
def monthly_roll(project, r_name, r_type, ex_acc, daterange, acc_template):
    cnt=0
    start_date, end_date = daterange.split(' - ')
    monthly_roll = [["Accounts", "Description"]]
    st_month=int(time.strftime("%m",time.strptime(start_date,"%Y-%m-%d %H:%M:%S")))
    st_year=int(time.strftime("%Y",time.strptime(start_date,"%Y-%m-%d %H:%M:%S")))
    en_month=int(time.strftime("%m",time.strptime(end_date,"%Y-%m-%d %H:%M:%S")))
    en_year=int(time.strftime("%Y",time.strptime(end_date,"%Y-%m-%d %H:%M:%S")))
    en_month+=(en_year-st_year)*12
    for x in range(st_month, en_month+1):
        if x > cnt*12 :
            monthly_roll[0].append(str(time.strftime('%B', time.struct_time((1, x-cnt*12, 0,)+(0,)*6)))+"_"+str(st_year+cnt))
        else:
            monthly_roll[0].append(str(time.strftime('%B', time.struct_time((1, x, 0,)+(0,)*6)))+"_"+str(st_year))
        if x%12==0:
            cnt+=1
    monthly_roll[0].append('Total')
    result = get_monthly_roll(project, r_name, r_type, ex_acc, daterange, acc_template)
    for resul in result:
        for res in resul:
            monthly_roll.append(list(res))
    cnt = total = 0
    total_month = []
    for mnr_acc in range(1,len(monthly_roll)):
        total_acc = 0
        total_month.append([])
        for mnr in range(2,len(monthly_roll[mnr_acc])):
            total_acc += is_none_value(monthly_roll[mnr_acc][mnr])
            total_month[cnt].append(is_none_value(monthly_roll[mnr_acc][mnr]))
        monthly_roll[mnr_acc].append(total_acc)
        total += total_acc
        cnt+=1
    monthly_roll.append(['Total', ''])
    for ttl in [sum(i) for i in zip(*total_month)]:
        monthly_roll[len(monthly_roll)-1].append(ttl)
    monthly_roll[len(monthly_roll)-1].append(total)
    file_path = export(project, r_name, r_type, monthly_roll, 2)
    return file_path

def get_monthly_roll(project, r_name, r_type, ex_acc, daterange, acc_template):
    #st_acc, en_acc = acc_template.split(' - ')
    start_date, end_date = daterange.split(' - ')
    st_month=int(time.strftime("%m",time.strptime(start_date,"%Y-%m-%d %H:%M:%S")))
    st_year=int(time.strftime("%Y",time.strptime(start_date,"%Y-%m-%d %H:%M:%S")))
    en_month=int(time.strftime("%m",time.strptime(end_date,"%Y-%m-%d %H:%M:%S")))
    en_year=int(time.strftime("%Y",time.strptime(end_date,"%Y-%m-%d %H:%M:%S")))
    en_month+=(en_year-st_year)*12
    s_a = selected_accounts(r_name, project)
    list_rep=[]
    result = get_object_or_404(Reports_type, name=r_type)
    if ex_acc == 'no_ex_acc':
        #dr-cr
        if r_name == 'CE' or r_name == 'GA' or r_name == 'Oth_exp' or r_name == 'LLR_exp' or r_name == 'SWAP_exp' or r_name == 'Int_exp' or r_name == 'CoS':
            for account in s_a[0][1].split(','):
                cnt=0
                sum_month = ""
                for x in range(st_month, en_month+1):
                    if x > cnt*12 :
                        sum_month += ", (coalesce((SELECT sum(amountncy) FROM turnovers_{id} WHERE dr = '{acc}' AND (Select Extract(MONTH from date_time)) = {mon} AND (Select Extract(YEAR from date_time)) = {year}),0) - coalesce((SELECT sum(amountncy) FROM turnovers_{id} WHERE cr = '{acc}' AND (Select Extract(MONTH from date_time)) = {mon} AND (Select Extract(YEAR from date_time)) = {year}), 0)) as {month}_{year}".format(id=project.id, acc=account.replace(' ', ''), mon=x-cnt*12, month=time.strftime('%B', time.struct_time((1, int(x-cnt*12), 0,)+(0,)*6)), year = st_year+cnt)
                    else:
                        sum_month += ", (coalesce((SELECT sum(amountncy) FROM turnovers_{id} WHERE dr = '{acc}' AND (Select Extract(MONTH from date_time)) = {mon} AND (Select Extract(YEAR from date_time)) = {year}),0) - coalesce((SELECT sum(amountncy) FROM turnovers_{id} WHERE cr = '{acc}' AND (Select Extract(MONTH from date_time)) = {mon} AND (Select Extract(YEAR from date_time)) = {year}), 0)) as {month}_{year}".format(id=project.id, acc=account.replace(' ', ''), mon=x, month=time.strftime('%B', time.struct_time((1, int(x), 0,)+(0,)*6)), year = st_year)
                    if x%12==0:
                        cnt+=1
                query = result.query.format(id=project.id, acc=account.replace(' ', ''), s_month=sum_month)
                list_rep.append(get_records(query))
        #cr-dr
        else:
            for account in s_a[0][1].split(','):
                cnt=0
                sum_month = ""
                for x in range(st_month, en_month+1):
                    if x > cnt*12 :
                        sum_month += ", (coalesce((SELECT sum(amountncy) FROM turnovers_{id} WHERE cr = '{acc}' AND (Select Extract(MONTH from date_time)) = {mon} AND (Select Extract(YEAR from date_time)) = {year}),0) - coalesce((SELECT sum(amountncy) FROM turnovers_{id} WHERE dr = '{acc}' AND (Select Extract(MONTH from date_time)) = {mon} AND (Select Extract(YEAR from date_time)) = {year}), 0)) as {month}_{year}".format(id=project.id, acc=account.replace(' ', ''), mon=x-cnt*12, month=time.strftime('%B', time.struct_time((1, int(x-cnt*12), 0,)+(0,)*6)), year = st_year+cnt)
                    else:
                        sum_month += ", (coalesce((SELECT sum(amountncy) FROM turnovers_{id} WHERE cr = '{acc}' AND (Select Extract(MONTH from date_time)) = {mon} AND (Select Extract(YEAR from date_time)) = {year}),0) - coalesce((SELECT sum(amountncy) FROM turnovers_{id} WHERE dr = '{acc}' AND (Select Extract(MONTH from date_time)) = {mon} AND (Select Extract(YEAR from date_time)) = {year}), 0)) as {month}_{year}".format(id=project.id, acc=account.replace(' ', ''), mon=x, month=time.strftime('%B', time.struct_time((1, int(x), 0,)+(0,)*6)), year = st_year)
                    if x%12==0:
                        cnt+=1
                query = result.query.format(id=project.id, acc=account.replace(' ', ''), s_month=sum_month)
                list_rep.append(get_records(query))
    else:
        #dr-cr
        if r_name == 'CE' or r_name == 'GA' or r_name == 'Oth_exp' or r_name == 'LLR_exp' or r_name == 'SWAP_exp' or r_name == 'Int_exp' or r_name == 'CoS':
            for account in s_a[0][1].split(','):
                cnt=0
                sum_month = ""
                for x in range(st_month, en_month+1):
                    if x > cnt*12 :
                        sum_month += ", (coalesce((SELECT sum(amountncy) FROM turnovers_{id} WHERE (dr = '{acc}' AND cr NOT IN({ex_acc})) AND (Select Extract(MONTH from date_time)) = {mon} AND (Select Extract(YEAR from date_time)) = {year}),0) - coalesce((SELECT sum(amountncy) FROM turnovers_{id} WHERE (cr = '{acc}' AND dr NOT IN({ex_acc})) AND (Select Extract(MONTH from date_time)) = {mon} AND (Select Extract(YEAR from date_time)) = {year}), 0)) as {month}_{year}".format(id=project.id, acc=account.replace(' ', ''), mon=x-cnt*12, month=time.strftime('%B', time.struct_time((1, int(x-cnt*12), 0,)+(0,)*6)), ex_acc=ex_acc, year = st_year+cnt)
                    else:
                        sum_month += ", (coalesce((SELECT sum(amountncy) FROM turnovers_{id} WHERE (dr = '{acc}' AND cr NOT IN({ex_acc})) AND (Select Extract(MONTH from date_time)) = {mon} AND (Select Extract(YEAR from date_time)) = {year}),0) - coalesce((SELECT sum(amountncy) FROM turnovers_{id} WHERE (cr = '{acc}' AND dr NOT IN({ex_acc})) AND (Select Extract(MONTH from date_time)) = {mon} AND (Select Extract(YEAR from date_time)) = {year}), 0)) as {month}_{year}".format(id=project.id, acc=account.replace(' ', ''), mon=x, month=time.strftime('%B', time.struct_time((1, int(x), 0,)+(0,)*6)), ex_acc=ex_acc, year = st_year)
                    if x%12==0:
                        cnt+=1
                query = result.query.format(id=project.id, acc=account.replace(' ', ''), s_month=sum_month)
                list_rep.append(get_records(query))
        #cr-dr
        else:
            for account in s_a[0][1].split(','):
                cnt=0
                sum_month = ""
                for x in range(st_month, en_month+1):
                    if x > cnt*12 :
                        sum_month += ", (coalesce((SELECT sum(amountncy) FROM turnovers_{id} WHERE (cr = '{acc}' AND dr NOT IN({ex_acc})) AND (Select Extract(MONTH from date_time)) = {mon} AND (Select Extract(YEAR from date_time)) = {year}),0) - coalesce((SELECT sum(amountncy) FROM turnovers_{id} WHERE (dr = '{acc}' AND cr NOT IN({ex_acc})) AND (Select Extract(MONTH from date_time)) = {mon} AND (Select Extract(YEAR from date_time)) = {year}), 0)) as {month}_{year}".format(id=project.id, acc=account.replace(' ', ''), mon=x-cnt*12, month=time.strftime('%B', time.struct_time((1, int(x-cnt*12), 0,)+(0,)*6)), ex_acc=ex_acc, year = st_year+cnt)
                    else:
                        sum_month += ", (coalesce((SELECT sum(amountncy) FROM turnovers_{id} WHERE (cr = '{acc}' AND dr NOT IN({ex_acc})) AND (Select Extract(MONTH from date_time)) = {mon} AND (Select Extract(YEAR from date_time)) = {year}),0) - coalesce((SELECT sum(amountncy) FROM turnovers_{id} WHERE (dr = '{acc}' AND cr NOT IN({ex_acc})) AND (Select Extract(MONTH from date_time)) = {mon} AND (Select Extract(YEAR from date_time)) = {year}), 0)) as {month}_{year}".format(id=project.id, acc=account.replace(' ', ''), mon=x, month=time.strftime('%B', time.struct_time((1, int(x), 0,)+(0,)*6)), ex_acc=ex_acc, year = st_year)
                    if x%12==0:
                        cnt+=1
                query = result.query.format(id=project.id, acc=account.replace(' ', ''), s_month=sum_month)
                list_rep.append(get_records(query))
    return list_rep

def summary(project, r_name, r_type, ex_acc, daterange, acc_template):
    if Reports_name.objects.filter(proffit_loss = True).filter(short_name = r_name).exists():
        if ex_acc == 'no_ex_acc':
            result = get_data_for_rep(project, r_name, r_type, 0, daterange, acc_template)
        else:
            result = get_data_for_report(project, r_name, r_type, 1, ex_acc, daterange, acc_template)
    else:
        result = get_data_for_rep(project, r_name, r_type, 0, daterange, acc_template)
    summary = [["Accounts", "Description", "Start", "DR", "CR", "End"]]
    total_start = total_dr = total_cr = total_end = 0;
    for resul in result:
        for res in resul:
            total_start += is_none_value(res[2])
            total_dr += is_none_value(res[3])
            total_cr += is_none_value(res[4])
            total_end += is_none_value(res[2]) + is_none_value(res[3]) - is_none_value(res[4])
            summary.append([res[0], res[1], is_none_value(res[2]), is_none_value(res[3]), 
                is_none_value(res[4]), is_none_value(res[2]) + is_none_value(res[3]) - is_none_value(res[4])])
    summary.append(['Total', '', total_start, total_dr, total_cr, total_end])
    file_path = export(project, r_name, r_type, summary, 3)
    return file_path

def brdn_all(project, r_name, r_type, materiality, ex_acc, daterange, acc_template):
    bal_sheet_arr = Reports_name.objects.filter(balance = True)
    proffit_loss_arr = Reports_name.objects.filter(proffit_loss = True)
    bal_list = []
    file_path = []
    proff_list = []
    for bal in bal_sheet_arr:
        bal_list.append(bal.short_name)
    for prof in proffit_loss_arr:
        proff_list.append(prof.short_name)
    if r_name == 'PPE' or r_name == 'Invent':
        file_path = brdn(project, r_name, r_type, materiality, 6, ex_acc, daterange, acc_template)
    elif r_name in proff_list:
        file_path = brdn_proff(project, r_name, r_type, materiality, 7, ex_acc, daterange, acc_template)
    elif r_name in bal_list and r_name != 'PPE' and r_name != 'Invent':
        additions = brdn(project, r_name, r_type, materiality, 4, ex_acc, daterange, acc_template)
        disposals = brdn(project, r_name, r_type, materiality, 5, ex_acc, daterange, acc_template)
        if additions[0]:
            file_path = [additions[0], additions[1]]
        elif disposals[0]:
            file_path = [disposals[0], disposals[1]]
        else:
            file_path = [False, "Additions: "+str(additions[1])+". Disposals: "+str(disposals[1])]
    return file_path

def brdn(project, r_name, r_type, materiality, i, ex_acc, daterange, acc_template):
    s_a = selected_accounts(r_name, project)
    accounts = str(s_a[0][1].replace(" ", "").split(','))[1:-1]
    brdn_ll = [["Date time", "Document", "Description", "Subconto", "DR", "CR", "AmountKGS", "AmountCCY", "CCY", "ABS", "CMA", "TS", "SFT"]]
    total_amountkgs = total_amountccy = total_abs = total_ts = total_sft = ts_sft = 0;
    result = get_breakDown(project, r_name, accounts, r_type, daterange, acc_template, materiality, i, ex_acc)
    for resul in result:
        for res in resul:
            res = list(res)
            total_amountkgs += is_none_value(res[6])
            total_amountccy += is_none_value(res[7])
            total_abs += is_none_value(res[9])
            ts_sft += is_none_value(res[9])
            if is_none_value(res[9]) >= materiality:
                ts_sft = ts_sft%float(materiality)
                total_ts += 1
                total_sft += 1
                brdn_ll.append([res[0], res[1], res[2], res[3], res[4], res[5], is_none_value(res[6]), 
                    is_none_value(res[7]), res[8], is_none_value(res[9]), ts_sft, "TS", "SFT"])
            elif ts_sft >= materiality:
                ts_sft = ts_sft%float(materiality)
                total_sft += 1
                brdn_ll.append([res[0], res[1], res[2], res[3], res[4], res[5], is_none_value(res[6]), 
                    is_none_value(res[7]), res[8], is_none_value(res[9]), ts_sft, "", "SFT"])
            else:
                brdn_ll.append([res[0], res[1], res[2], res[3], res[4], res[5], is_none_value(res[6]), 
                    is_none_value(res[7]), res[8], is_none_value(res[9]), ts_sft, "", ""])
    result = None
    brdn_ll.insert(0, ['Materiality', materiality])
    brdn_ll.insert(1, ['TS q-ty', total_ts])
    brdn_ll.insert(2, ['SFT q-ty', total_sft])
    brdn_ll.insert(3, [''])
    brdn_ll.append(['Total', '', '', '', '', '', total_amountkgs, total_amountccy, '', total_abs, '', total_ts, total_sft])
    file_path = export(project, r_name, r_type, brdn_ll, i)
    brdn_ll = None
    return file_path

def brdn_proff(project, r_name, r_type, materiality, i, ex_acc, daterange, acc_template):
    s_a = selected_accounts(r_name, project)
    #accounts = str(s_a[0][1].replace(" ", "").split(','))[1:-1]
    total_amountkgs = total_amountccy = total_abs = total_ts = total_sft = ts_sft = 0;
    brdn_ll = [["Date time", "Document", "Description", "Subconto1", "Subconto2", "DR", "CR", "AmountKGS", "AmountCCY", "CCY", "ABS", "CMA", "TS", "SFT"]]
    for account in s_a[0][1].split(','):
        accounts = '\''+str(account).replace(' ', '')+'\''
        result = get_breakDown(project, r_name, accounts, r_type, daterange, acc_template, materiality, i, ex_acc)
        for resul in result:
            for res in resul:
                res = list(res)
                total_amountkgs += is_none_value(res[7])
                total_amountccy += is_none_value(res[8])
                total_abs += is_none_value(res[10])
                ts_sft += is_none_value(res[10])
                if is_none_value(res[10]) >= materiality:
                    ts_sft = ts_sft%float(materiality)
                    total_ts += 1
                    total_sft += 1
                    brdn_ll.append([res[0], res[1], res[2], res[3], res[4], res[5], res[6], 
                        is_none_value(res[7]), is_none_value(res[8]), res[9], is_none_value(res[10]), ts_sft, "TS", "SFT"])
                elif ts_sft >= materiality:
                    ts_sft = ts_sft%float(materiality)
                    total_sft += 1
                    brdn_ll.append([res[0], res[1], res[2], res[3], res[4], res[5], res[6], 
                        is_none_value(res[7]), is_none_value(res[8]), res[9], is_none_value(res[10]), ts_sft, "", "SFT"])
                else:
                    brdn_ll.append([res[0], res[1], res[2], res[3], res[4], res[5], res[6], 
                        is_none_value(res[7]), is_none_value(res[8]), res[9], is_none_value(res[10]), ts_sft, "", ""])
                if str(res[6]) == str(res[5]):
                    total_amountkgs += is_none_value(res[7])
                    total_amountccy += is_none_value(res[8])
                    total_abs += is_none_value(res[10])
                    ts_sft += is_none_value(res[10])
                    if is_none_value(res[10]) >= materiality:
                        ts_sft = ts_sft%float(materiality)
                        total_ts += 1
                        total_sft += 1
                        brdn_ll.append([res[0], res[1], res[2], res[3], res[4], res[5], res[6], 
                        is_none_value(res[7])*-1, is_none_value(res[8]), res[9], is_none_value(res[10]), ts_sft, "TS", "SFT"])
                    elif ts_sft >= materiality:
                        ts_sft = ts_sft%float(materiality)
                        total_sft += 1
                        brdn_ll.append([res[0], res[1], res[2], res[3], res[4], res[5], res[6], 
                        is_none_value(res[7])*-1, is_none_value(res[8]), res[9], is_none_value(res[10]), ts_sft, "", "SFT"])
                    else:
                        brdn_ll.append([res[0], res[1], res[2], res[3], res[4], res[5], res[6], 
                        is_none_value(res[7])*-1, is_none_value(res[8]), res[9], is_none_value(res[10]), ts_sft, "", ""])
        result = None
    brdn_ll.insert(0, ['Materiality', materiality])
    brdn_ll.insert(1, ['TS q-ty', total_ts])
    brdn_ll.insert(2, ['SFT q-ty', total_sft])
    brdn_ll.insert(3, [''])
    brdn_ll.append(['Total', '', '', '', '', '', '', total_amountkgs, total_amountccy, '', total_abs, '', total_ts, total_sft])
    file_path = export(project, r_name, r_type, brdn_ll, i)
    brdn_ll = None
    return file_path

def get_breakDown(project, r_name, accounts, r_type, daterange, acc_template, materiality, i, ex_acc):
    start_date, end_date = daterange.split(' - ')
    st_acc, en_acc = acc_template.split(' - ')
    list_rep=[]
    if i == 4:
        for_split = 0
    elif i == 5:
        for_split = 1
    elif i == 6:
        for_split = 2
    elif i == 7:
        if ex_acc == 'no_ex_acc':
            if r_name == 'CE' or r_name == 'GA' or r_name == 'Oth_exp' or r_name == 'LLR_exp' or r_name == 'SWAP_exp' or r_name == 'Int_exp' or r_name == 'CoS':
                for_split = 6 #cr
            else:
                for_split = 5
        else:
            if r_name == 'CE' or r_name == 'GA' or r_name == 'Oth_exp' or r_name == 'LLR_exp' or r_name == 'SWAP_exp' or r_name == 'Int_exp' or r_name == 'CoS':
                for_split = 4 #cr
            else:
                for_split = 3
    result = get_object_or_404(Reports_type, name=r_type)
    query = result.query.split('|')[for_split].format(id=project.id, acc=accounts, mat=materiality, start=start_date, end=end_date, ex_acc=ex_acc)
    list_rep.append(get_records(query))
    return list_rep

def export(project, r_name, r_type, table_name, sheet_id):
    try:
        _sheet_name = sh_name(sheet_id)
        sheet_name = _sheet_name
        base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))+'/media/reports/project_'+str(project.id)
        if not os.path.exists(base_dir):
            os.makedirs(base_dir)
        xlsx_file = base_dir+'/'+r_name+'_report.xlsx'
        if os.path.isfile(xlsx_file):
            workbook = openpyxl.load_workbook(filename = xlsx_file)#, use_iterators = True)
            cnt = 1
            for worksheet in workbook.sheetnames:
                if str(worksheet) == str(sheet_name):
                    sheet = workbook[sheet_name]
                    workbook.remove(sheet)
                if str(worksheet) == str(sheet_name)+str(cnt):
                    sheet = workbook[str(sheet_name)+str(cnt)]
                    workbook.remove(sheet)
                cnt+=1
            if 'Sheet' in workbook.sheetnames:
                sheet = workbook['Sheet']
                workbook.remove(sheet)
        else:
            workbook = openpyxl.Workbook(xlsx_file)
            workbook.save(xlsx_file)
            workbook = openpyxl.load_workbook(filename = xlsx_file)
            if 'Sheet' in workbook.sheetnames:
                sheet = workbook['Sheet']
                workbook.remove(sheet)
        sheet = workbook.create_sheet(sheet_name)
        cnt_row_list = 0
        sheet_name_cnt = 1
        limit_value = 200000
        limit_count = limit_value
        for row_on_limit in table_name:
            if cnt_row_list == limit_count:
                styled_columns(sheet, sheet_name)
                sheet_name = str(_sheet_name)+'_'+str(sheet_name_cnt)
                sheet = workbook.create_sheet(sheet_name)
                sheet_name_cnt+=1
                limit_count += limit_value
            sheet.append(row_on_limit)
            cnt_row_list+=1
        styled_columns(sheet, sheet_name)
        workbook.save(xlsx_file)
        file_path = '/media/reports/project_'+str(project.id)+'/'+r_name+'_report.xlsx'
        return [True, file_path]
    except Exception as e:
        print("function export error: "+str(e))
        update_reports_document(r_name, 'Failed', str(e).replace("'", "\""), True, project)
        return [False, str(e).replace("'", "\"")]

def styled_columns(sheet, sheet_name):
    if 'Summary' in sheet_name or 'Monthly Roll' in sheet_name or 'Roll' in sheet_name or 'Register' in sheet_name:
        sheet.column_dimensions['A'].width = 12
        sheet.column_dimensions['B'].width = 30
        sheet.column_dimensions['C'].width = 14
        sheet.column_dimensions['D'].width = 14
        sheet.column_dimensions['E'].width = 14
        sheet.column_dimensions['F'].width = 14
        sheet.column_dimensions['G'].width = 14
        sheet.column_dimensions['H'].width = 14
        sheet.column_dimensions['I'].width = 14
        sheet.column_dimensions['J'].width = 14
        sheet.column_dimensions['K'].width = 14
        sheet.column_dimensions['L'].width = 14
        sheet.column_dimensions['M'].width = 14
        sheet.column_dimensions['N'].width = 14
        sheet.column_dimensions['O'].width = 20
    elif 'Additions & Disposlas' in sheet_name:
        sheet.column_dimensions['A'].width = 20
        sheet.column_dimensions['B'].width = 20
        sheet.column_dimensions['C'].width = 20
        sheet.column_dimensions['D'].width = 22
        sheet.column_dimensions['E'].width = 22
        sheet.column_dimensions['F'].width = 8
        sheet.column_dimensions['G'].width = 8
        sheet.column_dimensions['H'].width = 15
        sheet.column_dimensions['I'].width = 15
        sheet.column_dimensions['J'].width = 8
        sheet.column_dimensions['K'].width = 15
        sheet.column_dimensions['L'].width = 15
        sheet.column_dimensions['M'].width = 8
        sheet.column_dimensions['N'].width = 8
    elif 'Additions' in sheet_name or 'Disposlas' in sheet_name or 'BreakDown Net Movements' in sheet_name:
        sheet.column_dimensions['A'].width = 20
        sheet.column_dimensions['B'].width = 20
        sheet.column_dimensions['C'].width = 40
        sheet.column_dimensions['D'].width = 21
        sheet.column_dimensions['E'].width = 8
        sheet.column_dimensions['F'].width = 8
        sheet.column_dimensions['G'].width = 15
        sheet.column_dimensions['H'].width = 15
        sheet.column_dimensions['I'].width = 8
        sheet.column_dimensions['J'].width = 15
        sheet.column_dimensions['K'].width = 15
        sheet.column_dimensions['L'].width = 8
        sheet.column_dimensions['M'].width = 8
    else:
        sheet.column_dimensions['A'].width = 20
        sheet.column_dimensions['B'].width = 20
        sheet.column_dimensions['C'].width = 20
        sheet.column_dimensions['D'].width = 22
        sheet.column_dimensions['E'].width = 22
        sheet.column_dimensions['F'].width = 8
        sheet.column_dimensions['G'].width = 8
        sheet.column_dimensions['H'].width = 15
        sheet.column_dimensions['I'].width = 15
        sheet.column_dimensions['J'].width = 8
        sheet.column_dimensions['K'].width = 15
        sheet.column_dimensions['L'].width = 15
        sheet.column_dimensions['M'].width = 8
        sheet.column_dimensions['N'].width = 8
    for column_cells in sheet.columns:
        for cell in column_cells:
            if type(cell.value) == type(int(0)) or type(cell.value) == type(0.0) or type(cell.value) == type(decimal.Decimal(0)):
                cell.number_format = "### ### ### ##0"
    return True

def get_records(command):
    cur = None
    records = None
    try:
        cur = connection.cursor()
        cur.execute(command)
        records = cur.fetchall()
    except Exception as e:
        print("function get_records error: "+str(e))
        records = [False, str(e)]
    finally:
        if cur is not None:
        	cur.close()
    return records

def update_selected_accounts(name, accounts, project):
	command = (
		"""UPDATE reports_process_%d SET accounts='%s' WHERE report_name = '%s';""" % (project.id, accounts, name),
		)
	return execute(command)

def update_checked_reports(r_name, r_type, checked, project):
	command = (
		"""UPDATE reports_process_%d SET %s='%s' WHERE report_name = '%s';""" % (project.id, r_type, checked, r_name),
		)
	return execute(command)

def get_all_accounts(project_name, project_id):
	command = (
		"""SELECT * FROM chart_of_accounts_%s;""" % (project_id)
		)
	return get_records(command)

def selected_rep_name(project):
	command = (
		"""SELECT report_name FROM reports_process_%s where accounts != '';""" % (project.id)
		)
	return get_records(command)

def selected_accounts(short_name, project):
	command = (
		"""SELECT report_name, accounts FROM reports_process_%s where report_name = '%s';""" % (project.id, short_name)
		)
	return get_records(command)

def check_reports_name(project):
	rep_type = ""
	reports_type = Reports_type.objects.all()
	for r_t in reports_type:
		rep_type += "%s!='' OR "%r_t.name
	command = (
		"""SELECT * FROM reports_process_%s where %s;""" % (project.id, rep_type[:-3])
		)
	return get_records(command)

def get_data_for_report(project, r_name, r_type, i, ex_acc, daterange, acc_template):
	start_date, end_date = daterange.split(' - ')
	st_acc, en_acc = acc_template.split(' - ')
	s_a = selected_accounts(r_name, project)
	list_rep=[]
	result = get_object_or_404(Reports_type, name=r_type)
	for account in s_a[0][1].split(','):
		#query = result.query.split('|')[i].format(id=project.id, acc=account.replace(' ', ''), ex_acc=ex_acc, start=start_date, end=end_date, start_acc=st_acc, end_acc=en_acc)
		query = result.query.split('|')[i].format(id=project.id, acc=account.replace(' ', ''), ex_acc=ex_acc, start=start_date, end=end_date)
		list_rep.append(get_records(query))
	return list_rep

def get_data_for_rep(project, r_name, r_type, i, daterange, acc_template):
	start_date, end_date = daterange.split(' - ')
	st_acc, en_acc = acc_template.split(' - ')
	s_a = selected_accounts(r_name, project)
	list_rep=[]
	result = get_object_or_404(Reports_type, name=r_type)
	for account in s_a[0][1].split(','):
		#query = result.query.split('|')[i].format(id=project.id, acc=account.replace(' ', ''), start=start_date, end=end_date, start_acc=st_acc, end_acc=en_acc)
		query = result.query.split('|')[i].format(id=project.id, acc=account.replace(' ', ''), start=start_date, end=end_date)
		list_rep.append(get_records(query))
	return list_rep

def get_reports_document(project):
	command = (
		"""SELECT * FROM reports_process_%s WHERE file_add = True """ % project.id
		)
	return get_records(command)

def get_reports_transaction(project):
	command = (
		"""SELECT * FROM reports_process_{0} WHERE file_add = True AND report_name = '{1}';""".format(project.id, 'transactions')
		)
	return get_records(command)

def update_reports_document(r_name, file_path, err_mes, file_add, project):
	command = (
		"""UPDATE reports_process_{0} SET file_path = '{1}', err_message = '{4}', file_add = {2}, created_on = current_timestamp WHERE report_name = '{3}';""".format(project.id, file_path, file_add, r_name, err_mes),
		)
	return execute(command)

def get_account_template(project_id):
    command = (
        "SELECT case when exists (select dr from turnovers_{id} limit 1) then 'True' else 'False' end as template, (select dr from turnovers_{id} limit 1) as dr;".format(id=project_id)
        )
    return get_records(command)

def get_ex_acc(project_id):
    command = (
        "SELECT case when exists (select cr from turnovers_{id} where cr = '5999' limit 1) then 'True' else 'False' end as template, (select cr from turnovers_{id} where cr = '5999' limit 1) as acc;".format(id=project_id)
        )
    return get_records(command)

def sh_name(sheet_id):
	sheet_name_list = [
        'Register','Roll','Monthly Roll','Summary','Additions','Disposlas','BreakDown Net Movements',
        'Additions & Disposlas','Trans <0','Trans multiples of 100000','Trans first half year',
        'Trans count < 13','Trans last month day','Trans saturday','Trans sunday','Trans not working time',
        'Trans holidays','Trans adjustm,correct,reversal']
	return sheet_name_list[sheet_id]

def as_text(value):
    if value is None:
        return ""
    return str(value)

def is_none_value(value):
    if value is None:
        value = 0
        return value
    else:
        return value

#def remove_duplicates(values):
    #output = []
    #for value in values:
        #if value not in output:
            #output.append(value)
    #return output
