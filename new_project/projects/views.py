from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib import auth
from .models import Project, Document, Reports_name, Reports_type
from .forms import AddProjectForm, UploadDocumentForm
from .tasks import *
import json, itertools, zipfile, os, time, decimal, sys

def home(request):
    projects = Project.objects.filter(user_id = auth.get_user(request).id)
    username = auth.get_user(request)
    form_add_project = AddProjectForm
    context = {"projects" : projects, "username": username, "form_add_project": form_add_project}
    return render(request, 'projects/home.html', context)

def add_project(request):
    if request.POST:
        project = Project(title=request.POST['title_post'], user_id=auth.get_user(request).id)
        project.save()
        crt = create_tables(project)
        if crt == True:
            data = {'message': True, 'id': project.id, 'title': project.title}
        else:
            data = {'message': False, 'error': str(project.title)+" is not created. Error: "+str(crt)}
            project.delete()
        return HttpResponse(json.dumps(data), content_type='application/json')
    else:
        return HttpResponse(json.dumps({"nothing to see": "this isn't happening"}), content_type="application/json")

def upload_file(request, project_id):
    project = get_object_or_404(Project.objects.filter(user_id = auth.get_user(request).id), id=project_id)
    if request.method == 'POST':
        try:
            if is_archive(request.FILES['file_path']):
                form = UploadDocumentForm(request.POST, request.FILES, initial={"project": project})
                if form.is_valid():
                    form.save()
                    document = Document.objects.get(id=form.instance.id)
                    document.status  = "processing"
                    document.save()
                    extr_filename = extract_filename(document)
                    data = ['Success']
                    text = files = ""
                    for extr in extr_filename:
                        if extr[0] == "Error":
                            text = "File(s) upload erlier: "
                            files += extr[1]+" "
                            error = [text+files]
                        elif extr[0] == "Error1":
                            text = "File(s) not in archive: "
                            files += extr[1]+" "
                            error = [text+files]
                        elif extr[0] == "Error2":
                            error = [extr[1]]
                        else:
                            fileIDs = []
                            doc = Document(file_path = extr[0], project_id = project_id, table_name = extr[1])
                            doc.save()
                            data.append([doc.id, doc.status, doc.table_name, doc.file_path.url])
                            fileIDs.append(doc.id)
                            extract_file.delay(document, fileIDs)
                    if len(data)>1:
                        return HttpResponse(json.dumps(data), content_type="application/json")
                    else:
                        delete_archive(document)
                        return HttpResponse(json.dumps({'message': error}), content_type="application/json")
                else:
                    return  HttpResponse(json.dumps({'message', ['Form is invalid']}), content_type="application/json")
            else:
                return HttpResponse(json.dumps({'message', ['File is not an archive']}), content_type="application/json")
        except Exception as e:
            return HttpResponse(json.dumps({'message': str(e)}), content_type="application/json")
    else:
        form = UploadDocumentForm()
        return render(request, '/', {'form': form})

def delete_archive(document):
    file_path_archive = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))+"/"+str(document.file_path.url)
    if os.path.exists(file_path_archive):
        os.remove(file_path_archive)
        document.status = "delete"
        document.save()
        print(file_path_archive+" DELETED")
        return True
    else:
        print(file_path_archive+" is not exists")
        return False

def is_archive(path):
    document_name, document_ext = os.path.splitext(str(path))
    if document_ext == '.zip':
        return True
    else: 
        return False

def extract_filename(document):
    path_f = []
    try:
        filename_list_et = ['balance.csv', 'chart_of_accounts.csv', 'turnovers.csv']
        document_name, document_ext = os.path.splitext(document.filename())
        path = os.path.dirname(os.path.dirname(__file__))+document.file_path.url
        doc_name, doc_ext = os.path.splitext(document.filename())
        base_dir = os.path.dirname(os.path.dirname(__file__))+document.file_path.url.replace(document.filename(), doc_name)
        _zipfile = zipfile.ZipFile(path)
        filename_list = _zipfile.namelist()
        for name_list in filename_list_et:
            if name_list in filename_list:
                pathf = document.file_path.name.replace(document.filename(), document_name)
                name, ext = os.path.splitext(name_list)
                documents = Document.objects.filter(project_id=document.project_id).filter(table_name=name).filter(status='success')
                if documents:
                    for doc in documents:
                        if doc.table_name != name:
                            path_f.append([pathf+'/'+name_list, name])
                        else:
                            path_f.append(["Error", doc.table_name+".csv"])
                            print("Files "+doc.table_name+".csv"+" is upload erlier")
                else:
                    path_f.append([pathf+'/'+name_list, name])
                    print("Extract filename: "+name_list)
            else:
                path_f.append(["Error1", name_list])
                print("Files "+name_list+" not in archive")
    except Exception as e:
        path_f.append(["Error2", "function extract_filename error: "+str(e).replace("'", "\"")])
        print("function extract_filename error: "+str(e))
    finally:
        return path_f

def delete_project(request, project_id):
    if request.method == "POST":
        delete = Project.objects.get(id = project_id)
        delete_tables.delay(project_id)
        delete.delete()
        
        return HttpResponseRedirect('/..')    

def delete_file(request, doc_id):
    if request.method == "POST":
        doc = Document.objects.get(id=doc_id)
        doc.status = 'delete'
        doc.save()
        data = {'status': 'success'}
    else:
        data = {'status': 'error'}

    return HttpResponse(json.dumps(data), content_type='application/json')

def about(request):
    username = auth.get_user(request)
    context = {"username": username}
    
    return render(request, 'projects/about.html', context)

def admin(request):

    return render(request, 'admin')

def show_project(request, project_id):
    project = get_object_or_404(Project.objects.filter(user_id = auth.get_user(request).id), id=project_id)
    form = UploadDocumentForm(initial={"project": project})
    username = auth.get_user(request)
    try:
        documents_turn = Document.objects.filter(project = project_id).exclude(status = 'delete').filter(table_name = 'turnovers')
        documents_acc = Document.objects.filter(project = project_id).exclude(status = 'delete').filter(table_name = 'chart_of_accounts')
        documents_bal = Document.objects.filter(project = project_id).exclude(status = 'delete').filter(table_name = 'balance')
        bal_sheet_arr = Reports_name.objects.filter(balance = True)
        proffit_loss_arr = Reports_name.objects.filter(proffit_loss = True)
        reports_type_bal = Reports_type.objects.filter(balance = True)
        reports_type_proff = Reports_type.objects.filter(proffit_loss = True)
        all_accounts = get_all_accounts(project.title, project.id)
        account_template = [[None, None]]#get_account_template(project_id)
        #ex_acc = list(get_ex_acc(project_id)[0])
        s_r_n = selected_rep_name(project)
        ch_r_n = check_reports_name(project)
        rep_doc = get_reports_document(project)
        rep_trans = get_reports_transaction(project)
        context = {
            'documents_turn': documents_turn,
            'documents_acc': documents_acc,
            'documents_bal': documents_bal,
            'bal_sheet_arr': bal_sheet_arr,
            "proffit_loss_arr": proffit_loss_arr,
            "reports_type_bal": reports_type_bal,
            "reports_type_proff": reports_type_proff,
            "selected_rep_name": list(itertools.chain.from_iterable(s_r_n)),
            "check_reports_name": ch_r_n, "acc_temp": account_template[0][1],
            "reports_document": rep_doc, "reports_transaction": rep_trans,
            "project": project, "form": form, #"ex_acc": ex_acc,
            'all_accounts': all_accounts, "username": username
        }
        return render(request, "projects/project.html", context)
    except Exception as e:
        print("function show_project error: "+str(e))
        context = {
            'project': project,
            "form": form,
            "username": username
        }
        return render(request, "projects/project.html", context)

def accounts(request, project_id, reports_name_id):
    username = auth.get_user(request)
    project = get_object_or_404(Project.objects.filter(user_id = auth.get_user(request).id), id=project_id)
    reports_name = Reports_name.objects.get(id = reports_name_id)
    all_accounts = get_all_accounts(project.title, project.id)
    s_a = selected_accounts(reports_name.short_name, project)
    context = {
        'reports_name': reports_name,
        'all_accounts': all_accounts,
        'id': project_id,
        'selected_accs': s_a[0][1].split(','),
        "username": username
    }
    
    return render(request, "projects/accounts.html", context)

def add_accounts(request):
    if request.method == 'POST':
        project = get_object_or_404(Project.objects.filter(user_id = auth.get_user(request).id), id=request.POST['pr_id'])
        update_selected_accounts(request.POST['name'].replace('amp;',''), request.POST['accounts'], project);
        data = {'message': 'SUCCESS'}
        return HttpResponse(json.dumps(data), content_type='application/json')
    else:
        return HttpResponse(
            json.dumps({"error": "this isn't happening"}), content_type="application/json"
        )

def check_checkbox(request):
    if request.method == 'POST':
        project = get_object_or_404(Project.objects.filter(user_id = auth.get_user(request).id), id=request.POST['pr_id'])
        update_checked_reports(request.POST['r_name'], request.POST['r_type'], request.POST['r_checked'], project);
        data = {'message': 'SUCCESS'}
        return HttpResponse(json.dumps(data), content_type='application/json')
    else:
        return HttpResponse(
            json.dumps({"nothing to see": "this isn't happening"}), content_type="application/json"
        )

def start(request):
    if request.method == 'POST':
        project = get_object_or_404(Project.objects.filter(user_id = auth.get_user(request).id), id=request.POST['pr_id'])
        try:
            if request.POST['ex_acc'] != None and request.POST['ex_acc'] != 'no_ex_acc':
                ex_acc = str(request.POST['ex_acc'].replace(" ", "").split(','))[1:-1]#decimal.Decimal(request.POST['ex_acc'])
            else:
                ex_acc = request.POST['ex_acc']
            #print(ex_acc)
            #sys.exit(1)
            mat = decimal.Decimal(request.POST['materiality'])
            r_name = request.POST['r_name']
            r_type = request.POST['r_type']
            daterange = request.POST['date']
            acc_template = request.POST['acc_template']
            update_reports_document(request.POST['r_name'], 'Pending', '', True, project)
            data = {'message': 'OK', 'r_name': request.POST['r_name']}
            get_reports.delay(project, mat, ex_acc, r_name, r_type, daterange, acc_template)
            return HttpResponse(json.dumps(data), content_type='application/json')
        except Exception as e:
            print("function start error: "+str(e))
            update_reports_document(request.POST['r_name'], 'Failed', str(e).replace("'", "\""), True, project)
            data = {'message': 'ERROR', 'r_name': request.POST['r_name'], 'msg': str(e).replace("'", "\"")}
            return HttpResponse(json.dumps(data), content_type='application/json')
    else:
        return HttpResponse(
            json.dumps({"nothing to see": "this isn't happening"}), content_type="application/json"
        )

def start_transactions(request):
    if request.method == 'POST':
        project = get_object_or_404(Project.objects.filter(user_id = auth.get_user(request).id), id=request.POST['pr_id'])
        file = os.path.dirname(os.path.dirname(__file__))+"/media/reports/project_"+str(request.POST['pr_id'])+"/transactions_report.xlsx"
        if os.path.exists(file):
            os.remove(file)
        update_reports_document(request.POST['r_name'], 'Pending', '', True, project)
        transactions.delay('transactions', project)
        data = {'message': 'OK', 'r_name': "Transactions", 'file_path': ''}
        return HttpResponse(json.dumps(data), content_type='application/json')
    else:
        return HttpResponse(
            json.dumps({"nothing to see": "this isn't happening"}), content_type="application/json"
        )

def delete_transactions(request):
    if request.method == "POST":
        try:
            project = Project.objects.get(id=request.POST['pr_id'])
            file = os.path.dirname(os.path.dirname(__file__))+request.POST['tr_file_path']
            if os.path.exists(file):
                os.remove(file)
            update_reports_document(request.POST['tr_tbl_name'], '', '', False, project)
            data = {'status': 'success'}
        except Exception as e:
            print("function delete_trans error: "+str(e))
            data = {'status': 'error', 'msg': "function delete_trans error: "+str(e)}
        finally:
            return HttpResponse(json.dumps(data), content_type='application/json')