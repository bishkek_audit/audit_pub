# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models, migrations

def load_reports(apps, schema_editor):
	balance_sheet_arr = {"Cash|Cash and cash equivalents", "DfB|Due from banks", "TAR|Trade accounts receivable", 
		"LI|Loans issued", "LtC|Loans to customers", "IHM|Investments held to maturity", "IAS|Investments available for sale", 
		"FI|Financial instruments at fair value through profit or loss", "Invest|Investments", "AP|Advances paid", 
		"TA|Tax assets", "Invent|Inventories", "IP|Investment property", "DTA|Deferred tax assets", 
		"PPE|Property, plant and equipment", "IA|Intangible assets", "TAP|Trade accounts payable", "AR|Advances received", 
		"OE|Other payables and accrued expenses", "CD|Customer deposits", "Borrow|Borrowings and notes payable", 
		"SGF|Special government funds", "DB|Due to banks and financial institutions", "TP|Taxes payable", 
		"Grants|Grants received", "LR|Loan received", "DTL|Deferred tax liabilities", "SC|Share capital", "GR|General reserves", 
		"RR|Revaluation reserve", "APIC|Additionally paid-in-capital", "AD|Accumulated deficit", "RE|Retained earnings"}
	
	profit_and_loss_arr = {"Rev|Revenue", "Int_inc|Interest income", "CI|Commission income", "CoS|Cost of sales", 
		"CE|Commission expenses", "LLR_exp|Accrual of allowance for impairment losses on interest bearing assets", 
		"Int_exp|Interest expenses", "GA|General and administrative expenses", "Forex|Foreign exchange differences", 
		"Oth_inc|Other income", "Oth_exp|Other expenses", "Inc_tax|Income tax", 
		"SWAP_exp|Net gain/(loss) on operations with financial instruments"}
	
	Reports_name = apps.get_model("projects", "Reports_name")

	j=0
	for fl_nm in balance_sheet_arr:
		r_n = Reports_name(id=j, full_name=fl_nm.split('|')[1], short_name=fl_nm.split('|')[0], balance=True)
		r_n.save()
		j+=1
		
	l=33
	for fl_nm2 in profit_and_loss_arr:
		r_n = Reports_name(id=l, full_name=fl_nm2.split('|')[1], short_name=fl_nm2.split('|')[0], proffit_loss=True)
		r_n.save()
		l+=1

def delete_reports(apps, schema_editor):
    Reports_name = apps.get_model("Reports_name", "Reports_name")
    Reports_name.objects.all().delete()

class Migration(migrations.Migration):
    dependencies = [
        ('projects', '0001_initial'),
    ]
    operations = [
        migrations.RunPython(load_reports, delete_reports),
    ]