from django.conf.urls import url
from projects import views as projects_views
from django.views.generic import TemplateView
from django.conf.urls import include, url

urlpatterns = [
	url(r'^$', projects_views.home, name='home'),
 	url(r'^about/$', projects_views.about, name='about'),
 	url(r'^admin/$', projects_views.admin, name='admin'),
 	url(r'^projects/(?P<project_id>[0-9]+)$', projects_views.show_project, name='projects'),
 	url(r'^accounts/(?P<project_id>[0-9]+)/(?P<reports_name_id>[0-9]+)$', projects_views.accounts, name='accounts'),
 	url(r'^add_project$', projects_views.add_project, name='add_project'),
 	url(r'^delete_project/(?P<project_id>[0-9]+)$', projects_views.delete_project, name='delete_project'),
    url(r'^upload_file/(?P<project_id>[0-9]+)$', projects_views.upload_file, name='upload_file'),
 	url(r'^delete_file/(?P<doc_id>[0-9]+)$', projects_views.delete_file, name='delete_file'),
 	url(r'^add_accounts$', projects_views.add_accounts, name='add_accounts'),
 	url(r'^check_checkbox$', projects_views.check_checkbox, name='check_checkbox'),
 	url(r'^start$', projects_views.start, name='start'),
 	url(r'^start_transactions$', projects_views.start_transactions, name='start_transactions'),
 	url(r'^delete_transactions$', projects_views.delete_transactions, name='delete_transactions'),
]