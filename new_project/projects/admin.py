from django.contrib import admin
from projects.models import Project, Document, Reports_name, Reports_type

class AdminProject(admin.ModelAdmin):
    list_display = ["title", "user", "date"]
    ordering = ["title"]
    list_filter = ["date"]

    raw_id_list_displayfields = ('user')
    search_fields = ['title', 'user__username']

    def save_model(self, request, obj, form, change):
    	if form.is_valid():
    		if not request.user.is_superuser or not form.cleaned_data["user"]:
    			obj.user = request.user
    			obj.save()
    		elif form.cleaned_data["user"]:
    			obj.user = form.cleaned_data["user"]
    			obj.save()

    def preprocess_list_display(self, request):
    	if 'user' not in self.list_display:
    		self.list_display.insert(self.list_display.__len__(), 'user')
    	if not request.user.is_superuser:
    		if 'user' in self.list_display:
    			self.list_display.remove('user')

    def preprocess_search_fields(self, request):
    	if 'user__username' not in self.search_fields:
    		self.search_fields.insert(self.search_fields.__len__(), 'user__username')
    	if not request.user.is_superuser:
    		if 'user__username' in self.search_fields:
    			self.search_fields.remove('user__username')

    def changelist_view(self, request, extra_context=None):
    	self.preprocess_list_display(request)
    	self.preprocess_search_fields(request)
    	return super(AdminProject, self).changelist_view(request)

    def queryset(self, request):
    	if request.user.is_superuser:
    		return super(AdminProject, self).queryset(request)
    	else:
    		qs = super(AdminProject, self).queryset(request)
    		return qs.filter(user=request.user)

    def get_fieldsets(self, request, obj=None):
    	if request.user.is_superuser:
    		return super(AdminProject, self).get_fieldsets(request, obj)
    	return self.user_fieldsets

class AdminDocument(admin.ModelAdmin):
    list_display = ["project", "status", "file_path", "err_message", "created_on"]
    ordering = ["project"]
    list_filter = ["created_on"]

class AdminReports_name(admin.ModelAdmin):
    list_display = ["full_name", "short_name" , "balance", "proffit_loss"]
    ordering = ["full_name"]

class AdminReports_type(admin.ModelAdmin):
    list_display = ["name", "balance", "proffit_loss", "query"]
    ordering = ["name"]

admin.site.register(Project, AdminProject)
admin.site.register(Document, AdminDocument)
admin.site.register(Reports_name, AdminReports_name)
admin.site.register(Reports_type, AdminReports_type)