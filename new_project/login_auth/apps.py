from django.apps import AppConfig


class LoginAuthConfig(AppConfig):
    name = 'login_auth'
    verbose_name = 'Авторизация'
