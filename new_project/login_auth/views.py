from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import auth

def login(request):
	context = {}
	if request.method == "POST":
		username = request.POST.get('username', "")
		password = request.POST.get('password', "")
		user = auth.authenticate(username=username, password=password)
		if user is not None:
 			auth.login(request, user)
 			return HttpResponseRedirect('/')
		else:
 			login_error = "User not exist"
 			context = {"login_error": login_error}
 			return render(request, 'login_auth/login.html', context)
	else:
		print(request.user)
		if request.user.is_authenticated:
 			return HttpResponseRedirect('/')
		else:
			return render(request, 'login_auth/login.html', context)

def logout(request):
    auth.logout(request)
    return HttpResponseRedirect('/auth/login')